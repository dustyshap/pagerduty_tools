import os
import requests

from dotenv import load_dotenv


class PDOverride:
    def __init__(self):
        self.api_token = self._get_token()
        self.user = None

    @property
    def headers(self):
        return {
            "Accept": "application/vnd.pagerduty+json;version=2",
            "Authorization": f"Token token={self.api_token}",
            "Content-Type": "application/json"
        }

    @staticmethod
    def _get_token():
        load_dotenv()
        print("Attempting to read API Token from environment....")
        print("-------------------------------------------------")
        api_token = os.getenv("API_TOKEN")
        if api_token:
            print("API TOKEN FOUND!")
            print("-------------------------------------------------")
            return api_token
        else:
            print("ERROR: No API TOKEN found. Please check the environment file")
            print("-------------------------------------------------")
            quit()

    def authenticate_token(self):
        r = requests.get("https://api.pagerduty.com/users/me", headers=self.headers)
        if r.status_code == 200:
            self.user = r.json()['user']
            print(f"Authenticated as: {self.user['name']}")
            print("-------------------------------------------------")
        else:
            print("Token authentication failed! Please verify your API Credentials!")
            print("-------------------------------------------------")


if __name__ == '__main__':
    override = PDOverride()
    override.authenticate_token()

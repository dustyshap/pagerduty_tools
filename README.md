# PagerDuty Tools

Collection of tools/scripts used to programmatically do PagerDuty things via API

### Note
Eventually, this will include _actual_ tooling, but because I don't have a good sense for the way our schedules will be setup, and how we'll prefer to handle overrides, for now, it's simply a script that will authenticate your PagerDuty API Token, as well as providing instructions on how to generate your own API Token.

## Setup
1. Create your PagerDuty API User Token
   1. Login to PagerDuty
   2. In the top right, click on **My Profile**
   3. Under **User Settings**, click on **Create API User Token**
   4. Add a description in the box, and click **Create Token**.
2. Run `./script/setup`
3. Paste in your PagerDuty API User Token.
   1. This script will both set your API token in the environment, as well as install any dependencies that are needed.


## Authenticate
1. After setup, run `./script/override` (Ignore the name of the script, for now, as this will eventually include functionality to schedule overrides)
   1. The output will confirm that you've been authenticated and confirm your name, or fail, in which you should double-check the credential inputted.